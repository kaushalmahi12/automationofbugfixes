hostname = ""
port = 0
username = "xxxx"
password = "xxxxx"
database = "ninjapay"

customerWiseNinjapayTxnAmount = {}
ninjapay = {}
asgard = {}
customerWiseAsgardTxnAmount = {}
mismatchEntries = {}
wallet = {}

import mysql.connector
import datetime
myConnection = mysql.connector.connect( host=hostname, port=port, user=username, passwd=password, db=database )
conn2 = mysql.connector.connect( host=hostname, port=port, user=username, passwd=password, db=database )
cursor = myConnection.cursor()
cursor2 = conn2.cursor()

def fillCustomerWiseNinjapayTxnAmount(cursor):
    offset, limit = 0, 1000
    while True:
        query = '''select * from (select
            c.id as customerId, PrimaryAsgardUserId as asgardUserId
        from
            asgard.Customer c
        join asgard.AsgardUserToCustomerMap aucm ON aucm.customerId = c.id
            where c.deleted=0 and aucm.deleted=0
        group by c.id) userInfo
    join (select user_id, amount as walletBalance, id from wallets) wl on wl.user_id=userInfo.asgardUserId
    join (select date(created_at) as txn_date, amount, transaction_type, wallet_id from transactions) tr on tr.wallet_id = wl.id and tr.transaction_type="CREDIT" and tr.txn_date > "2019-06-01" limit {0}, {1} '''.format(offset, limit)
        cursor.execute(query)
        records = cursor.fetchall()
        if len(records) == 0:
            break
        print offset, ' ninja \n'
        offset += len(records)
        #if offset > 1600:
        #    break
        for x in records:
            customerId = int(x[0])
            txn_date = str(x[5])
            amount = float(x[6])
            walletBal = float(x[3])
            if not customerId in wallet:
                wallet[customerId] = walletBal
            if not customerId in customerWiseNinjapayTxnAmount:
                customerWiseNinjapayTxnAmount[customerId] = {}
            if not txn_date in customerWiseNinjapayTxnAmount[customerId]:
                customerWiseNinjapayTxnAmount[customerId][txn_date] = 0
            if not customerId in ninjapay:
                ninjapay[customerId] = 0
            ninjapay[customerId] += amount
            customerWiseNinjapayTxnAmount[customerId][txn_date] += amount



def fillCustomerWiseAsgardTxnAmount(cursor):
    offset, limit = 0, 1000
    while True:
        query = '''select inv.CustomerId as customerId, date(as_tr.createdAt), as_tr.Amount as asgardAmount from asgard.Invoice inv
    join asgard.Transactions as_tr on as_tr.invoiceId=inv.id where
    as_tr.PaymentType="WALLET" and as_tr.createdAt > "2019-06-01" limit {0}, {1}'''.format(offset, limit)
        cursor.execute(query)
        records = cursor.fetchall()
        if len(records)==0:
            break
        offset += len(records)
        print offset, ' asgard \n'
        for x in records:
            customerId = int(x[0])
            txn_date = str(x[1])
            amount = float(x[2])
            if not customerId in customerWiseAsgardTxnAmount:
                customerWiseAsgardTxnAmount[customerId] = {}
            if not txn_date in customerWiseAsgardTxnAmount[customerId]:
                customerWiseAsgardTxnAmount[customerId][txn_date] = 0
            if not customerId in asgard:
                asgard[customerId] = 0
            asgard[customerId] += amount
            customerWiseAsgardTxnAmount[customerId][txn_date] += amount


def fillMismatchEntries():
   # for customerId in customerWiseNinjapayTxnAmount:
   #     for dateStr in customerWiseNinjapayTxnAmount[customerId]:
   #         if customerId in customerWiseAsgardTxnAmount:
   #             if not customerId in mismatchEntries:
   #                 mismatchEntries[customerId] = {}
   #             if not dateStr in mismatchEntries[customerId]:
   #                 mismatchEntries[customerId][dateStr] = 0
   #             mismatchEntries[customerId][dateStr] = customerWiseNinjapayTxnAmount[customerId][dateStr] - (customerWiseAsgardTxnAmount[customerId][dateStr] if customerWiseAsgardTxnAmount[customerId][dateStr] != None else 0)

   # for key1 in mismatchEntries:
   #     for key2 in mismatchEntries[key1]:
   #         if mismatchEntries[key1][key2] > 0:
   #             print 'customerId: ', key1, 'amount: ', mismatchEntries[key1][key2]
   for customer in ninjapay:
       mismatchEntries[customer] = ninjapay[customer]
       if customer in asgard:
           mismatchEntries[customer] -= asgard[customer]
       if customer in wallet:
           mismatchEntries[customer] -= wallet[customer]
       if mismatchEntries[customer] > 0:
           print 'customerId: ', customer, 'mismatch amount: ', mismatchEntries[customer]

import uuid

#ninjapayThread = threading.Thread(target=fillCustomerWiseNinjapayTxnAmount, args=(cursor, ), name='ninjapayThread')
#asgardThread = threading.Thread(target=fillCustomerWiseAsgardTxnAmount, args=(cursor2, ), name='asgardThread')
#ninjapayThread.start()
#asgardThread.start()
#ninjapayThread.join()
#asgardThread.join()
#
##write data to file
#with open("data.txt", 'w') as f:
#    for customer in ninjapay:
#        f.write('customerId: {0}, np.transactionAmount: {1}'.format(customer, ninjapay[customer]))
#        if customer in wallet:
#            f.write(' walletBalance: {0}'.format(wallet[customer]))
#        if customer in asgard:
#            f.write(' asgardTransactionAmount: {0}'.format(asgard[customer]))
#        f.write('\n')
#
#fillMismatchEntries()
import requests
from requests.auth import HTTPBasicAuth

auth = HTTPBasicAuth('', '')

def total_invoice_value_of_invoice_with_id(inv_id):
    sql = '''select totalInvoiceValue from asgard.Invoice where id={}'''.format(inv_id)
    cursor.execute(sql)
    rec = cursor.fetchall()
    invoice_value = 0
    for entry in rec:
        invoice_value += float(entry[0])
    return invoice_value

def total_debited_amount_for(invoice_id):
    sql = '''select amount from transactions where ref_type="INVOICE" and transaction_type="DEBIT" and ref_id={}'''.format(invoice_id)
    cursor.execute(sql)
    rec = cursor.fetchall()
    debited_amount = 0
    for entry in rec:
        debited_amount += float(entry[0])
    return debited_amount

def credit_amount_to(userId, refId, amount):
    print 'crediting ', amount, ' for ', userId
    endpoint = "http://staging.ninjacart.in/payment/alpha/v1/wallet/{}/transaction".format(userId)
    resp = requests.post(endpoint, auth=auth, headers={"Content-Type": "application/json"}, json={\
	"amount": '{}'.format(amount),\
	"ref_type": "INVOICE",\
	"ref_id": '{}'.format(refId),\
	"transaction_type": "CREDIT",\
	"transaction_method": "CASH",\
        "created_by": 1,\
        "updated_by": 1,\
	"global_transaction_uuid": str(uuid.uuid1()),\
        "description": "reversing credit entry"\
        })
    if resp.status_code == 200:
        print 'all went ok for user:', userId, ' and refId: ', refId
    else:
        print 'something went wrong for user: ', userId, ' and refId: ', refId, resp.json()

def get_outstanding_invoice_ids_for(customerId):
    sql = '''select inv.id, so.type from asgard.Invoice inv join asgard.SaleOrder so on so.id=inv.saleOrderId where inv.customerId={} and inv.paymentStatus in (1, 2) and inv.outstandingAmount > 5'''.format(customerId)
    cursor.execute(sql)
    ids = []
    for x in cursor.fetchall():
        ids.append({"invoiceId": int(x[0]), "type": int(x[1])})
    return ids

def do_magic(customerID):
    print 'doing magic'
    endpoint = "http://5.9.1.86:8012/asgard/asgard_finance/transactions/nonFnv/raiseTransaction" #asgard endpoint to hit
    ids = get_outstanding_invoice_ids_for(customerID)
    orig_ids = []
    for entry in ids:
        orig_ids.append(entry["invoiceId"])
    print customerID, ': ', ids
    data = {"amountPaid": 0, "collectionDTOS": [{"amountPaid": 0, "invoiceId": x} for x in orig_ids]}
    resp = requests.post(endpoint, auth=auth, headers={"Content-Type": "application/json"}, json=data)
    if resp.status_code == 200:
        print 'magic happened for: ', customerID
    else:
        print 'something went wrong for: ', customerID, resp.json()

def print_wallet_balance_of(userId):
    endpoint = "http://staging.ninjacart.in/payment/alpha/v1/wallet/{}".format(userId)
    resp = requests.get(endpoint, auth=auth)
    print resp.json()


with open('input.txt') as f:
    cnt = 0
    for line in f:
        print '\n-----------------------------------------------------------------------------------------------------------------------------\n'
        line = line.split("\t")
        refId, customerId, userId = int(line[0]), int(line[2]), int(line[3])
        amount = total_debited_amount_for(refId) - total_invoice_value_of_invoice_with_id(refId)
        print 'amount: ', amount, 'userId: ', userId
        print '\n\nbefore credit: '
        print_wallet_balance_of(userId)
        cnt += 1
        try:
            credit_amount_to(userId, refId, amount)
        except:
            exit(1)
        print '\n\nafter credit: '
        print_wallet_balance_of(userId)
        do_magic(customerId)
        print '\n\nafter raising transactions: '
        print_wallet_balance_of(userId)
        print '\n-----------------------------------------------------------------------------------------------------------------------------\n'
cursor.close()
myConnection.close()
